import pandas as pd
from gooey import GooeyParser, Gooey
import codecs
import sys

if sys.stdout.encoding != 'UTF-8':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')
if sys.stderr.encoding != 'UTF-8':
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr.buffer, 'strict')


@Gooey(menu=[{'name': 'Ссылки', 'items': [{
    'type': 'Link',
    'menuTitle': 'Клиентский портал',
    'url': 'https://fuelnfleet.com/fleetsl/Orpak/Login.aspx?brand=orpak&slculture=ru-RU'
    }, {
        'type': 'Link',
        'menuTitle': 'Бэкофис',
        'url': 'https://fuelnfleet.com/amc/'
        }]},
             {'name': 'Помощь', 'items': [{
                 'type': 'AboutDialog',
                 'menuTitle': 'О программе',
                 'name': 'Автоматические отчёты',
                 'description': 'Программа для автоматизации отчётов и аналитики РГРАВТО.',
                 'version': '1.0',
                 'copyright': '2022',
                 'website': 'https://gitlab.com/ilyaiono/reports_rgr',
                 'developer': 'Ионов Илья\nilya.ionov@rgrauto.com',
             }, {
                 'type': 'MessageDialog',
                 'menuTitle': 'Функционал',
                 'message': f'''Данная программа генерирует различные отчёты для отправки клиентам, а также данные для внутренней аналитики.\n\n
                                    Boehringer:\n
                                    1 лист- одометр из разных источников для анализа работоспособности;\n
                                    2 лист- отчёт по пробегу для отправки в Leaseplan;\n
                                    3 лист- отчёт по Звёздам Безопасности для отправки в Boehringer.\n\n
                                    Maxxium:\n
                                    Отчёт по уровню топлива и пробегу на последний день месяца для отправки в Maxxium\n\n
                                    Danone:\n
                                    Отчёт по пробегу за месяц для отправки в Leaseplan.\n\n
                                    Чистка автопарка:\n
                                    Поиск устройств, находящихся в неверных флитах.
                                    ''',
                 'caption': 'Функционал'
             }]}],
       language='russian', program_name='Автоматические отчёты',
       sidebar_title='Доступные отчёты:', image_dir=r'C:\Users\User\Desktop\Отчёты\Reports_Project\images',
       default_size=(900, 650))
def my_parser():
    parser = GooeyParser(description='Скрипты для генерации отчётов (отправка клиентам) и внутренней аналитики.')
    subs = parser.add_subparsers(help='commands', dest='command')

    bi_parser = subs.add_parser('Boehringer')
    bi_parser.add_argument('status_file',
                           action='store',
                           widget='FileChooser',
                           help='Файл, который содержит актуальные статусы автопарка (запросить в Берингер)')
    bi_parser.add_argument('mobileye_file',
                           action='store',
                           widget='FileChooser',
                           help='Отчёт по Mobileye')
    bi_parser.add_argument('fleet_file',
                           action='store',
                           widget='FileChooser',
                           help='Выгрузка автопарка на конец месяца')
    bi_parser.add_argument('safety_stars_file',
                           action='store',
                           widget='FileChooser',
                           help='Отчёт по Звёздам Безопасности')
    bi_parser.add_argument('can_file',
                           action='store',
                           widget='FileChooser',
                           help='Отчёт по CAN-резюме')
    bi_parser.add_argument('last_month_file',
                           action='store',
                           widget='FileChooser',
                           help='Отчёт по пробегам за последний месяц (который мы отправляли)')
    bi_parser.add_argument('file_name',
                           action='store',
                           help='Имя нового отчёта')
    bi_parser.add_argument('save_dir',
                           action='store',
                           widget='DirChooser',
                           help='Папка, куда сохранить готовый файл')

    # ###################################################################
    fuel_parser = subs.add_parser('Maxxium')
    fuel_parser.add_argument('fuel_file',
                             action='store',
                             widget='FileChooser',
                             help='Отчёт по топливу')
    fuel_parser.add_argument('file_name',
                             action='store',
                             help='Имя нового отчёта')
    fuel_parser.add_argument('save_dir',
                             action='store',
                             widget='DirChooser',
                             help='Папка, куда сохранить готовый файл')

    # ###################################################################
    danone_parser = subs.add_parser('Danone')
    danone_parser.add_argument('last_send_file',
                               action='store',
                               widget='FileChooser',
                               help='Отчёт по пробегу, отправленный в прошлом месяце')
    danone_parser.add_argument('fleet_file',
                               action='store',
                               widget='FileChooser',
                               help='Выгрузка автопарка на конец месяца')
    danone_parser.add_argument('file_name',
                               action='store',
                               help='Имя нового отчёта')
    danone_parser.add_argument('save_dir',
                               action='store',
                               widget='DirChooser',
                               help='Папка, куда сохранить готовый файл')

    # ###################################################################
    clenup_parser = subs.add_parser('Чистка_автопарка')
    clenup_parser.add_argument('activity_report_file',
                               action='store',
                               widget='FileChooser',
                               help='Отчёт Activity report')
    clenup_parser.add_argument('file_name',
                               action='store',
                               help='Имя нового отчёта')
    clenup_parser.add_argument('save_dir',
                               action='store',
                               widget='DirChooser',
                               help='Папка, куда сохранить готовый файл')

    args = parser.parse_args()
    print(args, flush=True)
    print('=======================\n')
    return args


def bi_odometer(location):
    """
    Данный скрипт генерирует 3 листа:
    1. Внутренний отчёт по т/с Boehringer, где сравнивает пробеги из разных источников;
    2. Отчёт по пробегам для отправки в Leaseplan;
    3. Отчёт по Звёздам Безопасности для отправки в Boehringer.
    Перед использованием сохранить все загружаемые отчёты в формате .xlsx
    Перед загрузкой и сохранением оригинального файла из бэкофиса- ОБЯЗАТЕЛЬНО выставить американский формат даты!
    """

    def merger(get_col, left_key, right_key):
        """Функция объединяет пробеги по гос. номеру"""
        return inner_report.merge(get_col, how='left', left_on=left_key, right_on=right_key)

    print(f'''
    ******\n
    Данный скрипт генерирует 3 листа:\n
    1. Внутренний отчёт по т/с Boehringer, где сравнивает пробеги из разных источников;\n
    2. Отчёт по пробегам для отправки в Leaseplan;\n
    3. Отчёт по Звёздам Безопасности для отправки в Boehringer.\n
    ******\n
    ''', flush=True)
    print('Сравниваю пробеги...', flush=True)
    # Считываем статус тс (предоставляет Boehringer)
    status = reader(location.status_file, 0, '.')
    # Считываем отчёт Mobileye
    mobileye = reader(location.mobileye_file, 5, '.')
    # Считываем состояние автопарка
    fleet = reader(location.fleet_file, 0, '.')
    # Считываем звёзды безопасности
    safety_stars = reader(location.safety_stars_file, 5, ',')
    # Считываем CAN-резюме
    canbus = reader(location.can_file, 5, '.')
    # Считываем отчёт по пробегу за прошлый месяц (отправленный)
    last_month = reader(location.last_month_file, 0, '.')

    # Создаём новый df для отчёта
    inner_report = pd.DataFrame(columns=['Гос. Номер'])

    # Выбираем только машины BI из выгрузки автопарка (поиск по названию Fleet ID)
    i = 0
    for index, row in fleet.iterrows():
        if 'BOEH' in row['Fleet ID']:
            inner_report.loc[i, 'Гос. Номер'] = row['License Plate']
            i += 1

    # Подтягиваем данные из отчётов (везде поиск по Гос. Номеру)
    inner_report = merger(status['Статус'], ['Гос. Номер'], status['Гос Номер'])
    inner_report = merger(fleet['Fleet Name'], ['Гос. Номер'], fleet['License Plate'])
    inner_report = merger(last_month['Одометр'], ['Гос. Номер'], last_month['Гос. Номер'])
    inner_report = merger(fleet['Odometer (Kilometers)'], ['Гос. Номер'], fleet['License Plate'])

    inner_report.rename(columns={'Одометр': 'Одометр за прошлый месяц', 'Odometer (Kilometers)': 'Одометр текущий'},
                        inplace=True)
    inner_report['Одометр текущий'] = inner_report['Одометр текущий'].astype(float)
    inner_report['Разница'] = inner_report['Одометр текущий'] - inner_report['Одометр за прошлый месяц']

    inner_report = merger(canbus['Пробег (км)'], ['Гос. Номер'], canbus['Гос. Номер'])
    inner_report = merger(canbus['Расстояние (км)'], ['Гос. Номер'], canbus['Гос. Номер'])
    inner_report = merger(mobileye['Общий Пробег (км)'], ['Гос. Номер'], mobileye['Гос. Номер'])

    inner_report.rename(
        columns={'Пробег (км)': 'Одометр CAN', 'Расстояние (км)': 'Пробег CAN', 'Общий Пробег (км)': 'Пробег Mobileye'},
        inplace=True)
    inner_report = merger(safety_stars['Расстояние (км)'], ['Гос. Номер'], safety_stars['Гос. Номер'])
    inner_report.rename(columns={'Расстояние (км)': 'Пробег Звёзды Безопасности'}, inplace=True)

    # Новый df для отчёта по пробегу
    odometer_report = pd.DataFrame(columns=['Гос. Номер', 'Одометр', 'Клиент'])
    odometer_report['Гос. Номер'] = inner_report['Гос. Номер']
    odometer_report['Клиент'] = 'БЕНРИНГЕР ИНГЕЛЬХАЙМ'

    print('Подбираю оптимальный источник пробега...', flush=True)
    # Выбор оптимального источника данных одометра
    for index, row in inner_report.iterrows():
        # Если CAN отсутствует, то брать из портала
        if row['Одометр CAN'] == 0:
            odometer_report.loc[index, 'Одометр'] = row['Одометр текущий']

        # Если CAN соответствует порталу ИЛИ разница между CAN и порталом в пределах 15% (+/-), то брать CAN
        elif row['Одометр CAN'] == row['Одометр текущий'] or -15 <= (row['Одометр CAN'] - row['Одометр текущий']) / (
                row['Одометр CAN'] / 100) <= 15:
            odometer_report.loc[index, 'Одометр'] = row['Одометр CAN']

        # Если разница между CAN и порталом больше 15% (+/-), то делаем дальнейшую проверку:...
        elif -15 > (row['Одометр CAN'] - row['Одометр текущий']) / (row['Одометр CAN'] / 100) > 15:
            # Если разница между пробегом Mobiley(берёт с CAN) и пробегом из Звёзд(берёт с GPS) меньше 5%, то делаем дальнейшую проверку:...
            if (row['Пробег Mobileye'] - row['Пробег Звёзды Безопасности']) / (row['Пробег Mobileye'] / 100) <= 5:
                # Если разница между пробегом из Звёзд(берёт с GPS) и пробегом CAN больше 15%, то брать из портала
                if (row['Пробег Звёзды Безопасности'] - row['Пробег CAN']) / (
                        row['Пробег Звёзды Безопасности'] / 100) > 15:
                    odometer_report.loc[index, 'Одометр'] = row['Одометр текущий']
                # Если разница между пробегом из Звёзд(берёт с GPS) и пробегом CAN меньше или равна 15%, то брать CAN
                elif (row['Пробег Звёзды Безопасности'] - row['Пробег CAN']) / (
                        row['Пробег Звёзды Безопасности'] / 100) <= 15:
                    odometer_report.loc[index, 'Одометр'] = row['Одометр CAN']
            # Если разница между пробегом Mobiley(берёт с CAN) и пробегом из Звёзд(берёт с GPS) больше 5%, то брать из портала
            elif (row['Пробег Mobileye'] - row['Пробег Звёзды Безопасности']) / (row['Пробег Mobileye'] / 100) > 5:
                odometer_report.loc[index, 'Одометр'] = row['Одометр текущий']
    odometer_report['Одометр'] = odometer_report['Одометр'].astype(float)

    print('Строю отчёт по Звёздам Безопасности...', flush=True)
    # Отбираем в Звёздах Безопасности т/с с оценкой 3 и ниже
    safety_stars = safety_stars[safety_stars['Звезды'] <= 3].reset_index(drop=True)
    # Подтягиваем пробег CAN
    safety_stars = safety_stars.merge(canbus['Расстояние (км)'], how='left', left_on=['Гос. Номер'],
                                      right_on=canbus['Гос. Номер'])
    safety_stars.rename(columns={'Расстояние (км)_x': 'Расстояние (км)', 'Расстояние (км)_y': 'Пробег CAN'},
                        inplace=True)

    # Новый df для отчёта по Звёздам
    stars_report = pd.DataFrame(columns=safety_stars.columns)
    # Записываем в отчёт только те т/с, где нет CAN ИЛИ разница между пробегом в Звёздах и CAN в пределах 15% (+/-)
    for index, row in safety_stars.iterrows():
        if row['Пробег CAN'] == 0:
            stars_report.loc[index] = row
        elif -0.15 <= (row['Расстояние (км)'] - row['Пробег CAN']) / (row['Пробег CAN']) <= 0.15:
            safety_stars.loc[index, 'Разница в %'] = (row['Расстояние (км)'] - row['Пробег CAN']) / (row['Пробег CAN'])
            stars_report.loc[index] = row

    print('Сохраняю результаты...', flush=True)
    # Записываем результат в .xlsx
    with pd.ExcelWriter(f'{location.save_dir}\\{location.file_name}.xlsx') as writer:
        inner_report.to_excel(writer, sheet_name='Аналитика', index=False)
        odometer_report.to_excel(writer, sheet_name='БЕНРИНГЕР ИНГЕЛЬХАЙМ', index=False)
        stars_report.to_excel(writer, sheet_name='Звёзды', index=False)


def danone_odometer(location):
    """
    Данный скрипт генерирует отчёт по пробегу на последний день месяца (отправляем в Leaseplan по т/с Danone).
    Перед использованием сохранить выгрузку автопарка и CAN-резюме в формате .xlsx
    """
    print(
        '******\nДанный скрипт генерирует отчёт по пробегу на последний день месяца (отправляем в Leaseplan по т/с Danone).\n******\n',
        flush=True)
    print('Считываю пробег за месяц...', flush=True)

    # Считываем последний отправленный отчёт по пробегам
    odometer_old = reader(location.last_send_file, 0, '.')

    # Считываем выгрузку автопарка(на последний день месяца)
    fleet = reader(location.fleet_file, 0, '.')

    # Находим пробег по гос. номеру
    output = odometer_old.merge(fleet['Odometer (Kilometers)'],
                                how='left', left_on=['Гос. Номер'], right_on=fleet['License Plate'])

    # Меняем имена столбцов для лучшего восприятия
    output.rename(columns={'Одометр': 'Пробег из последнего отчёта', 'Odometer (Kilometers)': 'Пробег из автопарка'},
                  inplace=True)

    # Переводим значения в float, т.к. дробные числа считываются как string и с ними нельзя работать
    output['Пробег из автопарка'] = output['Пробег из автопарка'].astype(float)

    output['Пробег за месяц'] = output['Пробег из автопарка'] - output['Пробег из последнего отчёта']

    return output


def maxxium_fuel(location):
    """
    Данный скрипт генерирует отчёт по уровню топлива и пробегу на последний день месяца (Maxxium).
    Перед использованием сохранить отчёт по топливу в формате .xlsx
    Перед загрузкой и сохранением оригинального файла из бэкофиса- ОБЯЗАТЕЛЬНО выставить американский формат даты!
    """
    print(
        '******\nДанный скрипт генерирует отчёт по уровню топлива и пробегу на последний день месяца (Maxxium).\n******\n',
        flush=True)
    print('Подсчитываю топливо...', flush=True)

    # Считываем оригинальный файл, пропускаем ненужные колонки и убираем верхнюю "шапку" (5 строк)
    fuel_level = reader(location.fuel_file, 5, '.')

    # Находим уникальные гос.номера и последнюю дату события на каждый гос.номер
    unique = fuel_level.groupby(['Гос. Номер'])['Время События'].max()
    unique = unique.to_frame()

    # Соотносим оба массива для подстановки оставшихся колонок
    output = unique.merge(fuel_level, how='left', on=['Гос. Номер', 'Время События'])

    return output


def fleet_cleanup(location):
    """
    Данный скрипт генерирует отчёт по поиску устройств в неверных флитах.
    Перед использованием сохранить все отчёты в формате .xlsx
    Перед загрузкой и сохранением оригинального файла из бэкофиса- ОБЯЗАТЕЛЬНО выставить американский формат даты!
    """
    print('******\nДанный скрипт генерирует отчёт по поиску устройств в неверных флитах.\n******\n', flush=True)
    print('Просматриваю Activity Report...', flush=True)

    # Загружаем Activity Report из бэкофиса
    fleet = reader(location.activity_report_file, 0, '.')

    inactive = pd.DataFrame(columns=['Device ID', 'Fleet ID', 'Status', 'Deactivation Date'])
    active = pd.DataFrame(columns=['Device ID', 'Fleet ID', 'Status', 'Activation Date'])

    # Ищем устройства со статусом InActive, которые НЕ находятся в папке OLD
    i = 0
    for index, row in fleet.iterrows():
        if row['Current Status'] == 'InActive ' and 'OLD' not in row['Fleet ID']:
            inactive.loc[i, 'Device ID'] = row['Device ID']
            inactive.loc[i, 'Status'] = row['Current Status']
            inactive.loc[i, 'Deactivation Date'] = row['Deactivation Date']
            inactive.loc[i, 'Fleet ID'] = row['Fleet ID']
            i += 1
    # Сортировка по дате
    inactive = inactive.sort_values('Deactivation Date', axis=0, ascending=False)

    # Ищем устройства со статусом Active, которые находятся в папке NEW
    i = 0
    for index, row in fleet.iterrows():
        if row['Current Status'] == 'Active ' and 'NEW' in row['Fleet ID']:
            active.loc[i, 'Device ID'] = row['Device ID']
            active.loc[i, 'Status'] = row['Current Status']
            active.loc[i, 'Activation Date'] = row['Activation Date']
            active.loc[i, 'Fleet ID'] = row['Fleet ID']
            i += 1
    # Сортировка по дате
    active = active.sort_values('Activation Date', axis=0, ascending=False)

    print('Сортирую результаты...', flush=True)
    # Удаляем невидимый символ в конце ID
    inactive['Device ID'] = inactive['Device ID'].str.rstrip(' ')
    active['Device ID'] = active['Device ID'].str.rstrip(' ')

    print('Сохраняю файл...', flush=True)
    # Записываем результат в файл
    with pd.ExcelWriter(f'{location.save_dir}\\{location.file_name}.xlsx') as writer:
        inactive.to_excel(writer, sheet_name='InActive', index=False)
        active.to_excel(writer, sheet_name='Active', index=False)


def reader(file_name, skip, dec):
    """Функция считывает файлы .xlsx"""
    print('Читаю документ...', flush=True)
    return pd.read_excel(file_name, skiprows=skip, engine='openpyxl', decimal=dec)


def save_result(output, location):
    """Функция сохраняет результат в указанное место в .xlsx"""
    print('Сохраняю результаты...', flush=True)
    output.to_excel(f'{location.save_dir}\\{location.file_name}.xlsx', sheet_name='Отчёт', index=False)


if __name__ == '__main__':
    user_input = my_parser()
    if user_input.command == 'Maxxium':
        result = maxxium_fuel(user_input)
        save_result(result, user_input)
    elif user_input.command == 'Boehringer':
        result = bi_odometer(user_input)
    elif user_input.command == 'Danone':
        result = danone_odometer(user_input)
        save_result(result, user_input)
    elif user_input.command == 'Чистка_автопарка':
        fleet_cleanup(user_input)
